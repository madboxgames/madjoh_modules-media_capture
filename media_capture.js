define([
	'require',
	'madjoh_modules/spinner/spinner',
	'madjoh_modules/picture_resize/picture_resize',
	'madjoh_modules/styling/styling'
],
function(require, Spinner, PictureResize, Styling){
	var MediaCapture = {
		// ATTRIBUTES
			mediaPath : null,
			mediaFile : null,
			onSuccess : function(mediaPath, mediaFile){},
			onFailure : function(error){},
			user_cancelled : {
				code : -1,
				message : 'User Cancelled'
			},

		init : function(settings){
			MediaCapture.settings = settings;
		},

		// SHOW / CANCEL
			chooseInit : function(onSuccess, onFailure, options){
				MediaCapture.onSuccess = onSuccess;
				MediaCapture.onFailure = onFailure;

				MediaCapture.options = options ? options : {};
				if(!MediaCapture.options.compressProgress) MediaCapture.options.compressProgress = Spinner.onProgress;

				MediaCapture.mediaPath = null;
				MediaCapture.mediaFile = null;
			},
			show : function(onSuccess, onFailure, options){
				MediaCapture.chooseInit(onSuccess, onFailure, options);

				if(window.cordova) MediaCapture.settings.show(options);
				else MediaCapture.chooseFromFile();
			},
			cancelChooseMedia : function(e){
				var source = e.target;
				while(source.id !== 'media-selector-page'){
					if(source.id === 'choose-media-div') return;
					source = source.parentNode;
				}

				MediaCapture.settings.hide();
				MediaCapture.onFailure(MediaCapture.user_cancelled);
			},
			chooseOne : function(type, onSuccess, onFailure, options){
				MediaCapture.chooseInit(onSuccess, onFailure, options);

				if(window.cordova) MediaCapture[MediaCapture.typesMap[type]]();
				else MediaCapture.chooseFromFile();
			},

		// CHOOSE
			typesMap : {
				photo 	: 'choosePhoto',
				video 	: 'chooseVideo',
				gallery : 'chooseGallery'
			},
			choosePhoto : function(){
				navigator.camera.getPicture(MediaCapture.onChooseSuccess, MediaCapture.onChooseFailure, {
					sourceType 			: Camera.PictureSourceType.CAMERA,
					saveToPhotoAlbum 	: true
				});
			},
			chooseVideo : function(){
				if(MediaCapture.options.no_video) return;

				navigator.device.capture.captureVideo(MediaCapture.onChooseVideoSuccess, MediaCapture.onChooseFailure, {duration : 15});
			},
			chooseGallery : function(){
				navigator.camera.getPicture(MediaCapture.onChooseFromLibrarySuccess, MediaCapture.onChooseFailure, {
					sourceType 	: Camera.PictureSourceType.PHOTOLIBRARY,
					mediaType 	: MediaCapture.options.no_video ? Camera.MediaType.PICTURE : Camera.MediaType.ALLMEDIA
				});
			},
			chooseFromFile : function(){
				var onDialogClose = function(){
					window.removeEventListener('focus', onDialogClose, false);
					setTimeout(function(){MediaCapture.onChooseFromFileSuccess();}, 250);
				};
				window.addEventListener('focus', onDialogClose, false);

				Spinner.show();
				MediaCapture.fileInput = document.createElement('input');
				MediaCapture.fileInput.type = 'file';
				MediaCapture.fileInput.click();
				setTimeout(Spinner.hide, 2000);
			},

		// ON SUCCESS
			onChooseVideoSuccess : function(mediaFiles){
				console.log(JSON.stringify(mediaFiles));
				if(Styling.isAndroid()){
					VideoEditor.getVideoInfo(
						function(options){MediaCapture.compressVideo(mediaFiles[0].fullPath, options);},
						MediaCapture.onChooseFailure,
						{fileUri : mediaFiles[0].fullPath}
					);
				}else{
					MediaCapture.compressVideo(mediaFiles[0].fullPath);
				}
			},
			onChooseFromLibrarySuccess : function(mediaPath){
				console.log(mediaPath);
				var extension = mediaPath.split('.').reverse()[0].toLowerCase();
				var videos = ['mp4', 'mov'];
				if(videos.indexOf(extension) > -1 || mediaPath.indexOf('video') > -1){
					if(Styling.isAndroid() && mediaPath.indexOf('emulated') > -1 && mediaPath.indexOf('file') !== 0) mediaPath = 'file://' + mediaPath;
					MediaCapture.onChooseVideoSuccess([{fullPath : mediaPath}]);
				}
				else MediaCapture.onChooseSuccess(mediaPath);
			},
			onChooseFromFileSuccess : function(){
				var file = MediaCapture.fileInput.files[0];
				if(!file){
					MediaCapture.onFailure({
						code : -1,
						message : 'User Cancelled'
					});
					return;
				}
				
				if(file.type.match(/video.*/)) 	MediaCapture.mediaPath = URL.createObjectURL(file);
				else 							MediaCapture.mediaPath = file['name'];

				MediaCapture.limitWidth(file);
			},
			onChooseSuccess : function(mediaFiles){
				if(MediaCapture.duration){
					MediaCapture.showProgress(100);
					MediaCapture.duration = null;
				}
				console.log(mediaFiles);
				if(typeof mediaFiles !== 'string') MediaCapture.mediaPath = mediaFiles[0].fullPath;
				else MediaCapture.mediaPath = mediaFiles;

				PictureResize.URLtoBlob(MediaCapture.mediaPath, MediaCapture.limitWidth);
			},

		// ON FAILURE
			onChooseFailure : function(result){
				console.log(result);
				if(!result) 															return MediaCapture.onFailure(result);
				if(result === 'no image selected' || (result.code & result.code === 3)) return MediaCapture.onFailure(MediaCapture.user_cancelled);
				if(result.target && result.target.files && result.target.files[0]) 		return;

				MediaCapture.onFailure(result);
			},

		// LIMIT WIDTH
			limitWidth : function(blob){
				if(MediaCapture.options.maxWidth && parseInt(MediaCapture.options.maxWidth)){
					PictureResize.limitWidth(blob, MediaCapture.options.maxWidth, MediaCapture.updateMedia);
				}else{
					MediaCapture.updateMedia(blob);
				}
			},

		// COMPRESS VIDEO
			compressVideo : function(video, options){
				var videoFileName = 'justdare_video_' + Math.floor(Math.random() * 1000);

				MediaCapture.duration = null;
				if(options) console.log(JSON.stringify(options));

				var transcodeOptions = {
					fileUri 				: video, 
					outputFileName 			: videoFileName, 
					outputFileType 			: VideoEditorOptions.OutputFileType.MPEG4,
					optimizeForNetworkUse 	: VideoEditorOptions.OptimizeForNetworkUse.YES,
					progress 				: MediaCapture.options.compressProgress,
					saveToLibrary 			: false
				};

				if(Styling.isAndroid()) transcodeOptions.maintainAspectRatio = false
				if(options && options.width) 	transcodeOptions.width 	= options.width;
				if(options && options.height) 	transcodeOptions.height = options.height;

				VideoEditor.transcodeVideo(
					MediaCapture.onChooseSuccess,
					function(){
						console.log('Failed to compress video... Continuing with original file');
						MediaCapture.onChooseSuccess(video);
					},
					transcodeOptions
				);
			},

		// RETURN DATA
			updateMedia : function(blob){
				MediaCapture.mediaFile = blob;
				if(window.cordova && blob.type.match(/image.*/)){
					MediaCapture.mediaPath = URL.createObjectURL(blob);	
				}
				
				MediaCapture.onSuccess(MediaCapture.mediaPath, MediaCapture.mediaFile);

				if(window.cordova) MediaCapture.settings.hide();
			}
	};

	return MediaCapture;
});